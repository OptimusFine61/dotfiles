


from libqtile.config import Key, Group, Match, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile.lazy import lazy
from .keys import mod, keys

'''
groups = [Group(i) for i in [
    "  ", "  ", "  ", "  ", "󰙯  ", "  ", "  ", "  ", "󰡨  ", 
]]
'''

groups = [
        Group("   ",
              matches=None,
              exclusive=False,
              spawn="alacritty",
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group("  ",
              matches=None,
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group(" 󰖟 ",
              matches=[Match(wm_class="firefox")],
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group("  ",
              matches=[Match(wm_class="pcmanfm")],
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group(" 󰭹  ",
              matches=[Match(wm_class="telegram-desktop")],
              exclusive=False,
              spawn="telegram-desktop",
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group("   ",
              matches=[Match(wm_class="nomacs"),
                       Match(wm_class="gimp-2.10"),],
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group("   ",
              matches=[Match(wm_class="vlc")],
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group("   ",
              matches=None,
              exclusive=False,
              spawn=None,
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        Group(" 󰡨  ",
              matches=[Match(wm_class="docker desktop")],
              exclusive=False,
              spawn="docker desktop",
              layout="MonadTall",
              layouts=[],
              persist=True,
              init=True,
              layout_opts=None,
              screen_affinity=None,
              label=None
              ),
        ]

for i, group in enumerate(groups):
    actual_key = str(i+1)
    keys.extend([
        # Switch to workspace N
        Key([mod], actual_key, lazy.group[group.name].toscreen()),
        # Send window to workspace N
        Key([mod, "shift"], actual_key, lazy.window.togroup(group.name, switch_group=True))
    ])
